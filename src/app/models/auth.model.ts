export interface Credentials {
  email: string;
  password: string;
}

export interface AuthResponse {
  code: string;
  data: AuthToken;
  message: any;
}
export interface ErrorResponse {
  error: string;
}
export interface AuthToken {
  authToken: string;
}

// User Profile Interface

export interface ProfileResponse {
  code: string;
  data: Data;
  message: null;
}

export interface Data {
  guid: string;
  name: string;
  username: string;
  profile_photo_url: string;
  description: string;
  category: null;
  property_json: PropertyJSON;
  revision: string;
  vrsn: null;
  login_principal: any[];
  login_subject_ext: LoginSubjectEXT[];
}

export interface LoginSubjectEXT {
  guid: string;
  revision: null;
  vrsn: null;
  login_subject_guid: string;
  ext_type: string;
  ext_code: string;
  ext_option: string;
  property_json: JSON;
  param_code: string;
  param_name: string;
  param_type: string;
  param_option_json: JSON;
  value_string: string;
  value_file: null;
  value_numeric: null;
  value_datetime: null;
  value_json: null;
  created_date: string;
  updated_date: string;
  status: string;
}

export interface JSON {}

export interface PropertyJSON {
  dob: string;
  mobile: string;
  phone: string;
  gender: string;
  billing_address: BillingAddress;
  shipping_address: ShippingAddress[];
  orders: any[];
}

export interface BillingAddress {
  email: string;
  recipient: string;
  address: string;
  address2: string;
  country: string;
  state: string;
  zip: string;
  mobile: string;
}

export interface ShippingAddress {
  email: string;
  recipient: string;
  address: string;
  address2: string;
  country: string;
  state: string;
  zip: string;
  mobile: number;
}

//Permission Interface

export interface PermissionResponse {
  objectList: { [key: string]: null | string }[];
  totalRecords: null;
  offset: number;
  limit: number;
  code: string;
  message: null;
}
