import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import {
  AuthResponse,
  ProfileResponse,
  PermissionResponse,
  ErrorResponse
} from 'src/app/models/auth.model';

import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { SessionStore } from '../state/session.store';
import {tap, mergeMap} from 'rxjs/operators';
import {error} from '@angular/compiler/src/util';
import { ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  public form: FormGroup;
  userRole: string;
  token: string;
  code: string;
  toastNotification: string;

  constructor(
    private fb: FormBuilder,
    private service: AuthService,
    private sessionStore: SessionStore,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.token = localStorage.getItem('authToken');
    console.log(this.token);
    if (this.token) {
      this.router.navigate(['/home']);
    } else {
      this.router.navigate(['/login']);
    }
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  onSubmit() {
    // this.router.navigate(['/dashboards/dashboard1']);
    console.log(this.form.value.email);
    console.log(this.form.value.password);
    this.service.login(this.form.value).subscribe(
      (response: AuthResponse) => {
        this.toastr.success('You success to login', 'Congratulation',{
          tapToDismiss: true, progressBar: true, timeOut: 1300
        });
        localStorage.setItem('authToken', response.data.authToken);
        localStorage.setItem('email', this.form.value.email);
        // this.getPermissions();
        this.router.navigate(['/home']);


        // this.sessionStore.update({
        //   authToken: response.data.authToken,
        //   email: this.form.value.email
        // });
      },
      (error:any) => {
        if (error.error.code === 'CLIENT_AUTH_WRONGPASSWORD') {
          this.toastNotification = 'The password that you insert are not correct';
        }
        if (error.error.code === 'CLIENT_AUTH_TOOMANYFAILEDATTEMPTS') {
          this.toastNotification = 'Sorry, You are request login too many time, Please try again.';
        }
        if (error.error.code === 'CLIENT_AUTH_WRONGEMAIL') {
          this.toastNotification = 'The email that you insert are not correct';
        }
        this.toastr.error(this.toastNotification, 'Error',{
          tapToDismiss: true, progressBar: true
        });
        console.log('Error:::', error.error.code);
      }
    );
  }

  getPermissions() {
    this.service
      .getProfile()
      .pipe(
        tap((x: ProfileResponse) => {
          this.sessionStore.setLoading(true);
          localStorage.setItem('guid', x.data.guid);

          //Store in store :)
          this.sessionStore.update({
            guid: x.data.guid
          });
        }),
        mergeMap(x => this.service.getPermission(x.data.guid))
      )
      .subscribe(
        (response: PermissionResponse) => {
          if (response.objectList.length) {
            console.log('You are admin');
            this.userRole = 'ADMIN';
            localStorage.setItem('role', this.userRole);
            this.sessionStore.setLoading(false);
            this.sessionStore.update({ role: this.userRole });
            this.router.navigate(['/home']);
          } else {
            console.log('You are awesome');
            this.userRole = 'CUSTOMER';
            localStorage.setItem('role', this.userRole);
            this.sessionStore.update({ role: this.userRole });
            this.sessionStore.setLoading(false);
          }
        },
        error => {
          this.sessionStore.setError(error);
          console.log('Error:::', error);
        }
      );
  }
}
