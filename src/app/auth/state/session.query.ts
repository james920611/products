import {Query} from '@datorama/akita';
import {SessionStore, SessionState} from './session.store';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionQuery extends Query<SessionState> {
  loggedIn$ = this.select(state => !!state.authToken);
  email$ = this.select('email');
  role$ = this.select('role');
  guid$ = this.select('guid');
  token$ = this.select('authToken');
  admin$ = this.select(state => !!(state.role === 'ADMIN'));

  constructor(protected store: SessionStore) {
    super(store);
  }
}
