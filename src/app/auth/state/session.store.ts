import { Store, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface SessionState {
  authToken: string;
  email: string;
  role: string;
  guid: string;
}

export function createInitialState(): SessionState {
  return {
    authToken: '',
    email: '',
    role: '',
    guid: ''
  };
}

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {
  constructor() {
    super(createInitialState());
  }
}
