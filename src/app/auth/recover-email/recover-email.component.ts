import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import {
  AuthResponse,
  ProfileResponse,
  PermissionResponse,
  ErrorResponse
} from 'src/app/models/auth.model';

import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { SessionStore } from '../state/session.store';
import {tap, mergeMap} from 'rxjs/operators';
import {error} from '@angular/compiler/src/util';
import { ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-recover-email',
  templateUrl: './recover-email.component.html',
  styleUrls: ['./recover-email.component.css']
})
export class RecoverEmailComponent implements OnInit {
  hide = true;
  confirmpasswordhide= true;
  public form: FormGroup;
  userRole: string;
  token: string;
  code: string;
  toastNotification: string;

  constructor(
    private fb: FormBuilder,
    private service: AuthService,
    private sessionStore: SessionStore,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      code: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      confirmpassword: [null, Validators.compose([Validators.required])]
    });
  }

}
