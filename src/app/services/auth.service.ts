import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Credentials } from '../models/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}

  get Token() {
    return {
      headers: new HttpHeaders({
        Authorization: localStorage.getItem('authToken'),
        'Content-Type': 'application/json',
        tenantCode: '',
        appId: '10'
      })
    };
  }

  get Headers() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        tenantCode: '',
        appId: '10'
      })
    };
  }

  login(credentials: Credentials) {
    const URL = 'https://api.akaun.com/core1/auth/login';

    return this.http.post(URL, credentials, this.Headers);
  }

  getProfile() {
    const URL = 'https://api.akaun.com/core1/userprofile';
    return this.http.get(URL, this.Token);
  }

  getPermission(guid: string) {
    const URL = `https://api.akaun.com/core1/perm-assignee-target-links/query?perm-dfn-guid=C95A86C8-2E1D-443D-B32B-B82A45045863&assignee-hdr-guid=${guid}`;
    return this.http.get(URL, this.Token);
  }
}
