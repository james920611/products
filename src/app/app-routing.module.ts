import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import {ForgotpassComponent} from './auth/forgotpass/forgotpass.component';
import {HomeComponent} from './home/home.component';
import {JamesComponent} from './auth/james/james.component';
import { RecoverEmailComponent } from './auth/recover-email/recover-email.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'forgotpass',
    component: ForgotpassComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'james',
    component: JamesComponent
  },
  {
    path: 'recovery-email',
    component: RecoverEmailComponent
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
